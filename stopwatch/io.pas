uses crt, sysutils;

procedure initTimer(var initHH, initMM: integer);
begin
  clrscr;
  write('Set timer (HH MM): ');
  read(initHH, initMM);
end;

procedure drawTime(HH, MM, SS: Word);
begin
  clrscr;
  if KeyPressed then
    if ReadKey = #27 {Esc} then
      Halt (1);
  gotoXY(1,1);
  writeln(format('%d:%d:%d',[hh,mm,ss]));
end;

procedure quit;
begin
  clrscr;
end;
