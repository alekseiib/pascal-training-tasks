program stopwatch;
{$I io.pas}

var
  initMM, initHH: integer;
  startTime, diffTime, initTime: TDateTime;
  HH,MM,SS,MS: Word;
const
  delayMS: Word = 10;

begin
  initTimer(initHH, initMM);
  initTime := EncodeTime(initHH,initMM,0,0);
  startTime := Time;
  repeat
    Delay(delayMS);
    diffTime := Time - startTime;
    DecodeTime(initTime - diffTime,HH,MM,SS,MS);

    drawTime(HH,MM,SS);
  until diffTime >= initTime;
  quit;
end.
